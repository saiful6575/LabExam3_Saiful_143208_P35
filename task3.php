<?php
$low = 2;
$high = 1000;

$file = fopen("PrimeNumber.txt", "w");

while ($low < $high)
{
    $flag = 0;

    for($i = 2; $i <= $low/2; ++$i)
    {
        if($low % $i == 0)
        {
            $flag = 1;
            break;
        }
    }

    if ($flag == 0) {
        fwrite($file, $low);
        fwrite($file, "\n");
    }

    ++$low;
}

fclose($file);